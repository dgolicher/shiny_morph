library(rgdal)
library(graticule)
#library(ggmap)
library(raster)
library(dismo)
library(rgeos)
library(insol)

lon<--92
lat<-55
startyr<-2000
startmn<-1
jd<-1:350


days<-input$G5
start_date<-sprintf("%04d/%02d/%02d",startyr,startmn,1)
sun<-sunrise.set(33.43, -84.22, start_date, timezone="UTC+5",nu=100)
sun$sunrise$date
sun<-data.frame(dt=-sun$sunrise$date,sun$sunset,sun$sunrise)
g0<-ggplot(sunrise,aes(x=date,y=sunrise))
g1<-g0+geom_line()  
g1 







days<-100
start_date<-as.Date(sprintf("%04d-%02d-%02d",startyr,startmn,1))
end_date<-start_date+days
day_seq<-as.POSIXct(seq(start_date,end_date,1))
d<-sunriset(lon.lat,as.POSIXct(day_seq),direction="sunrise",POSIXct.out=TRUE) 
str(d)
start_date<-as.Date(sprintf("%04d-%02d-%02d",startyr,startmn,1))
end_date<-start_date+100

mp <- gmap("Cold bay", zoom=11)

grid <- graticule(lons = seq(mp@extent@xmin,mp@extent@xmax,gridsize*1000), 
                  lats = seq(mp@extent@ymin,mp@extent@ymax,gridsize*1000), tiles = TRUE)
proj4string(grid)<-proj4string(mp)
dd<-mp<100
a<-extract(dd,grid)
grid@data$ht<-unlist(lapply(a,mean))
grid_points<-data.frame(coordinates(grid),ht=grid@data$ht)

# plot(mp)
# plot(grid,add=T)
# #points(coordinates(grid),pch=21,bg="red")


# names(grid_points)<-c("x","y","ht")
# for(i in 1:20){
#   Sys.sleep(0.1)
#   grid_points$ht<-sample(grid_points$ht,length(grid_points$ht))
#   points(grid_points$x,grid_points$y,pch=21,bg=grid_points$ht)
# }

